*** Microsoft Azure for Students ***
    https://azure.microsoft.com/pt-br/offers/ms-azr-0144p/

user: zaqueu.antonio@cursos.univesp.br
pwd: novedoisR@@t.debian.Root

1 - Cloud shell
2 - Create a virtual machine (My first cloud virtual machine on azure)
- usr: zaqueur | pwd: 92Z@.azure
- preço: 0,40 reais/h
- login: ssh -i zaqueur zaqueur@52.149.142.189
- systemctl poweroff
- APAGAR A MÁQUINA DEPOIS DO USO PARA EVITAR COBRANÇAS...

3 - Criar grupo de recursos
4 - Azure monitor
5 - Serviços de kubernetes
    4.1 - Install ***Kubernetes Services Cluster
    4.2 - az login
    4.3 - az aks get-credentials --resource-group cluster-k8s-testing(resource-name) --name k8s(cluster-name)
    4.4 - kubectl get pods --all-namespaces
    4.5 - kubectl apply -f .\k8s-namespace.yaml
    4.6 - kubectl get namespaces
    4.7 - helm repo add bitnami https://charts.bitnami.com/bitnami
    4.8 - helm install mongodb -n data-bases -f .\k8s-helm-mongodb.yaml bitnami/mongodb  --version 10.3.3
            NAME: mongodb
            LAST DEPLOYED: Thu Jan 28 11:55:40 2021
            NAMESPACE: data-bases
            STATUS: deployed
            REVISION: 1
            TEST SUITE: None
            NOTES:
            ** Please be patient while the chart is being deployed **

            MongoDB can be accessed on the following DNS name(s) and ports from within your cluster:

                mongodb.data-bases.svc.cluster.local

            To get the root password run:

                export MONGODB_ROOT_PASSWORD=$(kubectl get secret --namespace data-bases mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)

            To get the password for "app" run:

                export MONGODB_PASSWORD=$(kubectl get secret --namespace data-bases mongodb -o jsonpath="{.data.mongodb-password}" | base64 --decode)

            To connect to your database, create a MongoDB client container:

                kubectl run --namespace data-bases mongodb-client --rm --tty -i --restart='Never' --env="MONGODB_ROOT_PASSWORD=$MONGODB_ROOT_PASSWORD" --image docker.io/bitnami/mongodb:4.4.3-debian-10-r0 --command -- bash

            Then, run the following command:
                mongo admin --host "mongodb" --authenticationDatabase admin -u root -p $MONGODB_ROOT_PASSWORD

            To connect to your database from outside the cluster execute the following commands:

            NOTE: It may take a few minutes for the LoadBalancer IP to be available.
                    Watch the status with: 'kubectl get svc --namespace data-bases -w mongodb'

                export SERVICE_IP=$(kubectl get svc --namespace data-bases mongodb --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}")
                mongo --host $SERVICE_IP --port 27017 --authenticationDatabase admin -p $MONGODB_ROOT_PASSWORD
    4.9 - helm uninstall mongodb --namespace=data-bases
    4.10 - kubectl get pods --all-namespaces
    4.11 - kubectl get svc --namespace=data-bases
    4.12 - kubectl -n data-bases exec -ti mongodb-7cd9994cd4-8zhnq bash

6 - Backup automatico
7 - Resources healths
8 - Calculadora de preços:
    https://azure.microsoft.com/pt-br/pricing/calculator/?service=kubernetes-service
9 - Velero
    https://www.youtube.com/watch?v=zybLTQER0yY
    https://velero.io/

    schedules = horário
    premises = instalações
