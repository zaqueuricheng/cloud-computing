* [x] Stop Cluster staging/dev pelo portal do Azure
* [x] Subir uma VM no GCP Cloud (Minha conta) para rodar o script
* [x] Subir um cluster de teste no GCP Cloud (Minha conta)
* [ ] Stop/Start Cluster staging/dev pelo console do GCP Cloud
* [ ] Auto "Stop/Start" Cluster staging/dev usando shell script (GCP Cloud)
* [ ] Conhecer os comandos básicos do GCP Cloud cli
* [ ] Usar o scrip em uma VM e agendar a automação com crontab das 7h-18h
* [ ] Adequação do "velero backup" para GCP Cloud
* [ ] Install chatbot server (helm/chart)
* [ ] IaC - Terraform/Ansible e Gitlab