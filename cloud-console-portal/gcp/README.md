## Curso de GCP
    - https://www.youtube.com/watch?v=dn9cSRImmVA

## Instalação do cli
    - https://chocolatey.org/packages/gcloudsdk
    - https://cloud.google.com/sdk/docs/install

## 
    - https://cloud.google.com/sdk/docs/components

## Bootcamp - Básico - Fundamentos do GCP: Big Data & Machine Learning
    - https://www.youtube.com/watch?v=sOx6NO22W_A

## Criação de projetos
    - Introdução a computação em nuvem
        - https://docs.microsoft.com/pt-br/learn/modules/fundamental-azure-concepts/
    - Maquina virtual no GCP
        Para criar máquina virtual (VM) no GCP primeiro cria-se o PROJETO q é paracido com RESOURCE GROUP do Azure e posteriormente criar as INSTÂNCIAS (VMs).
        - Conexão geração de ssh-key
            - https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys?_ga=2.171705385.-848743761.1616417608&_gac=1.50547291.1616607786.CjwKCAjwxuuCBhATEiwAIIIz0WRltaKklhmmrPpoYNusKjJOBgeCqItRw5gmBprf_rtsGGgx72pdExoC5h4QAvD_BwE#windows

            - Instalar o putty
            - Use o PuTTYgen
                - Gera a chave (chave, user, password)
                - Salva a chave em modo privado e publico opcionalmente
                - Copia a chave e cola no console em: compute engine->metadata
                - Copia o IP Externo da VM e cola no Putty
                - No Pytty vai em ssh->Auth->Seleciona o arquivo de autenticação
                - Basta conectar e adicionar seu usuário e senha
                - C:Users/User/.ssh/known_hosts ## Para limpar os hosts de autenticação

    - Kubernetes em nuvem
        - EKS (Elastic Kubernetes Service)
        - AKS (Azure Kubernetes Service)
        - GKE (Google Kubernetes Engine)
    - Instalação de virtual machine
        - Ambiente grafico
        - Terraform e Ansible
    - Instalação de cluster <>
        - Ambiente grafico
        - Terrafrom e Ansible

## Preços das VMs (Mês/Hora)
https://cloud.google.com/compute/all-pricing?hl=pt-br

## Diferença de gerações de processadores
https://razorcomputadores.com.br/blog/hardware/processadores-intel/

## Desenvolvedor FullStack
https://www.igti.com.br/blog/como-ser-um-desenvolvedor-full-stack/

## Kubernetes
https://www.cienciaedados.com/kubernetes-pods-nodes-containers-e-clusters/#:~:text=Kubernetes%20%C3%A9%20um%20sistema%20de,de%20aplicativos%20baseados%20em%20microsservi%C3%A7os.