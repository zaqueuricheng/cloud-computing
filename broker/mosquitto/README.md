broker
- https://engprocess.com.br/mosquitto/

MQTT - Protocolo de comunicação para IoT.
Mosquitto - É um dos componentes do protocolo. Também chamado como broker, é um intermediario entre máquinas e protocolos para fazer com que os aparelhos possam conversar entre si e agir de maneira automatizada.