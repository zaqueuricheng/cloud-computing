https://www.youtube.com/watch?v=UCn3kTEMfCM

IaC - Infraestruture as Code

Terraform/Azure
Terraform é uma ferramenta openSource que permite construir e versionar a infraestrura de forma segura.

Basics Concepts
    - Terraform files
    - Providers -  São as conexões com os provedores
    - Resource - 
    - Initialization
    - Apply - constroi a infra
    - State
    - Variables
    - Provisioner
    - Destroy - destoi toda a infra

Install
    - choco install terraform

Basics Commands
    - terraform
    - terraform --version