## Docs: https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/azure_cli

# 1 - Login in your azure account
    ## az login 
    ## az account list // Use this if you have any azure account
    ## az account set --subscription="<subscription_id>" // Use this command if you have any azure subscription

# 2 - Configure the azure provider
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = "fe0f8c3a-1822-4eee-8e70-826d8af32379" // This is optional field, when you make az login
}

# 2.1 - Create a resource group

resource "azurerm_resource_group" "rg" {
    name = "rg-terraform-test" // Nome do seu grupo de recursos
    location = "eastus2"       // Localização do grupo de recursos, location can be
                               // <southcentralus> <westus> <brazilsouth>
        
        ## Criar tags no resource-group <rg-terraform-test>
        tags = {
            environment = "Production" 
            team = "DevStaging"
        }
}

# 3 - Use this comands to create a resourse group in your account
    ## terraform init // Cria o arquivo <.terraform.lock.hcl> que faz a instalação do provider e configura
                      // Cria também o diretorio .terraform onde tem um executável

    ## terraform plan // 
    ## terraform init -upgrade
    ## terraform apply // Cria o grupo de recursos no seu provedor

# 4 - Criar tags em "resource" function and run the commands bellow
     ## terraform plan --out=plan1 // Segundo versionamento
     ## terraform apply
     ## terraform show // Mostra a última configuração feita

# 5 - Create a virtual network and subnet
resource "azurerm_virtual_network" "vnet"{
    name                = "vn-terraform"
    address_space       = ["10.1.0.0/16"]
    location            = "eastus2"
    resource_group_name = azurerm_resource_group.rg.name // Onde a rede sera criada
}

resource "azurerm_subnet" "subnet"{
    name = "internal"
    resource_group_name = azurerm_resource_group.rg.name
    virtual_network_name = azurerm_virtual_network.vnet.name
    address_prefix = "10.1.1.0/24"
}

## terraform init
## terraform plan
## terraform apply
        ## in azure search <virtual-network> // Aqui verás a rede criada
        ## In left menu click in <subnet> para ver a subnet criada

## 6 - Destroy all resources created
    ## terraform plan -destroy // Verifica o que vai ser destruido
    ## terraform destroy