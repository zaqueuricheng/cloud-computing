https://www.youtube.com/watch?v=UCn3kTEMfCM

IaC - Infraestrutura como código - Transformar nossa infraestrutura em código a fim de fazer o deployment da sua infra
    - Versionamento e documentação da sua infraestrutura
    - Escalabilidade da sua infra
    - Terraform foi desenvolvida com a linguagem golang

# Get azure parameters
    az login
    $subscription_id = az account list --query '[?isDefault].id' -o tsv
    $tenant_id = az account list --query '[?isDefault].tenantId' -o tsv
# Configure the azure provider
    provider "azurerm"{
        version = "~>1.32.0"
        use_msi = true
        subscription_id = ""
        tenant_id = ""
    }

# Install plug-ins
    - Azure Terraform
    - Terraform
# Docs
    - https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs