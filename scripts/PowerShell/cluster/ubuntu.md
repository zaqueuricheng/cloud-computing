
## Install gcloud command on ubuntu
- sudo snap install google-cloud-sdk --classic 

## Login in gcp account
- gcloud auth login // Go to the link generated with this command in your browser, to get the verification code

## Set the project
- gcloud projects list
- gcloud config set project <PROJECT_ID>

## List components and update
- gcloud components list
- gcloud components update

## List, create, stop, start and delete VMs
- gcloud compute instances list

## List, create, describe, resize and delete CLUSTERS
- gcloud container clusters list
- gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --machine-type g1-small
- gcloud container clusters describe $CLUSTER_NAME
- gcloud container clusters resize $CLUSTER_NAME --size=0

## Create my cluster with linux terminal example | Install kubectl
- export CLUSTER_NAME="String" // democlustercli
- export CLUSTER_ZONE="String" // us-central1-a | https://cloud.google.com/compute/docs/regions-zones#available
- export CLUSTER_REGION="String" // us-central1, us-east1 | https://cloud.google.com/compute/docs/regions-zones

- echo $CLUSTER_NAME
- gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --zone $CLUSTER_ZONE --machine-type g1-small

## My script to schedule the cluster
- gcloud container clusters resize $CLUSTER_NAME --size=0 --zone $CLUSTER_ZONE // Script 1: stop_cluster
- gcloud container clusters resize $CLUSTER_NAME --size=1 --zone $CLUSTER_ZONE // Script 2: start_cluster
- yes Y | gcloud container clusters resize $CLUSTER_NAME --size=0 --zone $CLUSTER_ZONE

    ## Sript
    - touch start_cluster
    - chmod +x start_cluster
    - /home/node-01/start_cluster
    - cp start_cluster /home/node-01/stop_cluster # Copia e cria novo arquivo a partir do original

    - /bin/sh /home/node-01/stop_cluster
    ## crontab

- https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
- snap install kubectl --classic


- grep CRON /var/log/syslog

- vim /etc/crontab