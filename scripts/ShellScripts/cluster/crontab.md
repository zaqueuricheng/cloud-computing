https://crontab.guru/monday-to-friday
https://crontab.guru/examples.html

https://kvz.io/schedule-tasks-on-linux-using-crontab.html
https://medium.com/totvsdevelopers/entendendo-o-crontab-607bc9f00ed3
https://dev.to/pauld/automate-start-and-stop-of-google-cloud-compute-engine-22a8

apt install cron

# Answer for any question to solve the problem
https://stackoverflow.com/questions/30823604/google-cloud-sdk-code-to-execute-via-cron
https://stackoverflow.com/questions/7642674/how-do-i-script-a-yes-response-for-installing-programs/7642711
https://stackoverflow.com/questions/33873177/google-cloud-platform-logging-in-to-gcp-from-commandline

# MINUTOS HORA DIA_DO_MES MES DIA_DA_SEMANA COMANDO
- 0-59    0-23   1-31     1-12    0-7       BASH MEU_SCRIPT
# 2h:00 | todos dia do mês | todos meses | de seg. a sex.
- 0 2 * * 1-5 bash meu_script.sh
# 5min | horario comercial | todos dias do mês | mês de jan, jul e dez | de seg. a sex.
- */5 8-18 * 1,7,12 1-5 bash meu_script.sh