# Install gcloud command on ubuntu
- sudo snap install google-cloud-sdk --classic 

# Login in gcp account
- gcloud auth login // Go to the link generated with this command in your browser, to get the verification code
- gcloud auth login --no-launch-browser
# Set the project
- gcloud projects list
- gcloud config set project <PROJECT_ID>

# List virtual machine and clusters
- gcloud compute --help # Create and manipulate compute engine resources
- gcloud compute instances list

- gcloud container --help # Deploy and manage clusters of machines for running containers
- gcloud container clusters list

# List components and update
- gcloud components list
- gcloud components update

# Create my GKE cluster with linux terminal
- export CLUSTER_NAME="your-cluster-name" // democlustercli
- export CLUSTER_ZONE="zone" // us-central1-a | us-east1
- export CLUSTER_REGION="region"

- echo $CLUSTER_NAME

- gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --zone $CLUSTER_ZONE --machine-type g1-small

# Install kubectl to get/list/describe k8s resources
- https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
- snap install kubectl --classic

# Resize nodes to stop/start resources
- gcloud container clusters resize $CLUSTER_NAME --size=0 --zone $CLUSTER_ZONE
- gcloud container clusters resize $CLUSTER_NAME --size=1 --zone $CLUSTER_ZONE 
- yes Y | gcloud container clusters resize $CLUSTER_NAME --size=0 --zone $CLUSTER_ZONE

# Create script file and read 
- touch start_cluster
- chmod +x start_cluster
- /home/node-01/start_cluster
- cp start_cluster /home/node-01/stop_cluster
- /bin/sh /home/node-01/stop_cluster

# Crontab
- crontab -e
- crontab -l
- sudo vim /etc/crontab

## read the script for every minute
- */1 * * * * /home/node-01/start_cluster.sh

# Cron do windows???