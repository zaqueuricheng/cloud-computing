#!/bin/bash

# forcing the profile in my script to run gcloud command
source /etc/profile

# logs to monitore time that the service was started
echo "Start cluster works!" >> /home/node-01/start_works.txt
date >> /home/node-01/start_works.txt
echo "" >> /home/node-01/start_works.txt
echo "#####################################################" >> /home/node-01/start_works.txt
echo "" >> /home/node-01/start_works.txt

# variables
AUTH_USER=""
AUTH_PWD=""
PROJECT_ID=""
CLUSTER_NAME="democlustercli"
CLUSTER_ZONE="us-central1-a"
CLUSTER_REGION=""

# command to resize cluster for start all pods and services
yes Y | gcloud container clusters resize $CLUSTER_NAME --size=1 --zone $CLUSTER_ZONE