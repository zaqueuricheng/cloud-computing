1 - Criar o script
2 - Instalar o gcloud command em outra maquina linux
3 - Logar com seu usuário do GCP
4 - Testar o script
5 - Agendar tarefa com o crontab para automatizar a leitura do script

6 - GCP Commands to Stop/Start VM-Instances
    - gcloud auth login
    - gcloud projects list ## Lista todos projetos 
    - gcloud compute instances stop vm-instance-1 --project <PROJECT_ID> ## Para uma instancia de um determinado projeto, pode expecificar a zona também.
    - gcloud compute instances start vm-instance-1 --project virtual-machines-308616

7 - GCP Commands to Stop/Start Clusters
    - gcloud list components
    - gcloud components update
    - gcloud install component 
    - gcloud compute regions list
    - gcloud beta dataproc clusters stop staging-dev --project virtual-machines-308616 --region=us-central1-b ## Precisará habilitar o dataproc
    - gcloud beta dataproc clusters start <cluster-name> --region=region --project <project-id>

    - gcloud dataproc clusters describe cluster-1 --region us-central1
    - gcloud beta dataproc clusters stop cluster-dev --region southamerica-east1 --project servers-308616

****************************** INIT
- gcloud auth login
- gcloud projects list
- gcloud config set project <PROJECT_ID>

- gcloud list components
- gcloud components update

***************************** VMS
- gcloud compute regions list // Lista as regiões onde serão instaladas as vms
- gcloud compute instances list // Lista as vms da instaladas


*********************************** WAYS TO CREATE CLUSTERS ON GCP ***************************************
- gcloud init --help

****************************************** CONTAINER CLUSTER ******************************************
- gcloud auth login
- gcloud projects list
- gcloud config set project <clusters-308916>
- gcloud components update
- gcloud container clusters list
- gcloud container clusters --help // create and delete cluster only

https://stackoverflow.com/questions/38323134/how-to-stop-gcloud-container-engine-clusters
- $CLUSTER_NAME = "clusterdemocli"
- gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --machine-type g1-small // Criar um cluster com configurações padrão
- gcloud container clusters list
- gcloud container clusters describe $CLUSTER_NAME
- gcloud container clusters resize $CLUSTER_NAME --size=0 // Redimencionar o cluster para 0 nodes, ajuda diminuir o custo

****************************************** DATAPROC CLUSTER ******************************************
- gcloud dataproc clusters --help // create, delete, stop and start cluster
- gcloud dataproc clusters create my_cluster --region=us-central1
- gcloud dataproc clusters start my-cluster --region us-east1

************** DATAPROC
    https://www.youtube.com/watch?v=wSdFwZFik6A
    
    https://www.youtube.com/watch?v=SN2VDCBzSlE
    
    https://www.cetax.com.br/blog/apache-hadoop/

    https://techcrunch.com/2019/09/10/google-brings-cloud-dataproc-to-kubernetes/

    https://stackoverflow.com/questions/46436794/what-is-the-difference-between-google-cloud-dataflow-and-google-cloud-dataproc

    https://stackoverflow.com/questions/48574676/how-to-stop-or-shut-down-a-google-dataproc-cluster

    https://stackoverflow.com/questions/54083394/problem-with-google-cloud-dataproc-clusters-create-properties-tag

    https://www.youtube.com/watch?v=1GnguWjEVvM

    https://www.youtube.com/watch?v=STrtHeBV_98

    
    - snap install kubectl --classic
    - spark-shell

    gcloud container clusters get-credentials <cluster-name> --zone <zone> --project <project_id>

    gcloud dataproc clusters get-iam-policy <cluster-name> --region <region>