https://www.youtube.com/watch?v=K9GfM-dVr2w


# APLICAÇÕES
- Monolítico (Para monitorar essas aplicações usava-se "Nagios", "Zabbix", "Prometheus", "Grafana")
    - User interface
    - Business logic
    - Data interface (API)
    - Data bases

- Microserviços (Arquiteturas modernas) - Controlar estes tipos de arquiteturas é muito complexo, aqui entra o conceito de service mesh.
    - Service Mesh - É uma forma de controlar requisições, metricas e "algumas bagunças" em seus microserviços
    - Istio - É um conjunto de softwares ou serviços que vai trazer o "Service Mesh" para você. Falar de Istio é falar de vários componentes como: Prometheus (metricas), Grafana, Pilot (), Galley (), Kiali (maneira grafica para ver como seus serviços estão rodando e interagindo entre si), Citadel, Mixer e algumas estrategias de deployment. Esses serviços já vêm instalados como default no istio.

- "QUALQUER COISA QUE VOCÊ QUEIRA FAZER NA VIDA A PRIMEIRA COISA A SE FAZER É ACESSAR A PÁGINA PRODUTO - DOCUMENTAÇÃO OFICIAL"

# Istio
- https://www.youtube.com/watch?v=16fgzklcF7Y
- Istio is a Service Mesh
- Service Mesh manages communicatin between microservices

## Configure Istio and Istio Components
    - Istio is configured with Kubernetes YAML files 
    - Istio use Kubernetes CustomResourceDefinitions (CRD)
        - extending the kubernetes API
    - Istiod

## Instalação
- gcloud auth login
- gcloud projects list
- gcloud config set project <PROJECT_ID>

- gcloud compute --help
- gcloud compute instances list

- gcloud container clusters list

- $CLUSTER_NAME="democlustercli"
- $CLUSTER_ZONE="us-central1-a"
- $PROJECT_ID="velerodemo"

- gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --zone $CLUSTER_ZONE --machine-type e2-standard-2

- https://cloud.google.com/compute/docs/machine-types

- gcloud container clusters get-credentials $CLUSTER_NAME --zone $CLUSTER_ZONE --project $PROJECT_ID

## Istio é uma forma de você controlar os seus micro serviços, imagina que você tem uma quantidade de micro serviços "Pods/Aplicações" rodando dentro do seu cluster kubernetes. Nela você consegue ver quais aplicações estão conversando, observabilities, gerenciamento de tráfego, segurança.

- Linux dicas - Criar cluster com máquinas virtuais (Aulão do Jefferson)
    - Terminais sincronizados 
    - 1 - Criar três instancias (Hyper-v, GCP, Azure, outra cloud)
    - 2 - Conectar as máquinas localmente
    - 3 - Mudar o nome das máquinas
        - sudo su
        - hostname elliot-01 // Muda o nome da máquina
        - echo elliot-01 >> /etc/hostname // Rediciona o nome da máquina para o arquivo hostname e já muda o nome no terminal
        - bash
    - 4 - Instalar o docker // Ver doc
    - 5 - Instalar o Kubernetes // Ver doc. apt-get install kubelet kubectl(gerenciar) kubeadm(criar cluster)
        - Configurar o cluster (Em uma das instancias, no caso a master)
            - 5.1 - kubeadm config images pull // Faz o download de todas as imagens q ele precisa para fazer funcionar o cluster
            - 5.2 - kubeadm init // Faz a inicialização do kubernetes
            - // Como criar mult-master de k8s
            - Siga as instruções de saída do comando "kubeadm init"
    - 6 - Convida as outras instancias
    - 7 - Vá para o site da doc: https://istio.io/latest/docs/setup/getting-started/ e leia
    - 8 - Instalar o istioclt para o seu SO // Leia a documentação, entenda um pouco
        - 8.1 - Usando o comando istioctl
            - istioctl
            - istioctl install --set profile=demo -y
            - kubectl label namespace default istio-injection=enabled

            - kubectl get namespaces default -o yaml
            - kubectl get services -n istio-system // Mostra o loadbalancer para acessar o istio

            - kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
            - kubectl get gateway